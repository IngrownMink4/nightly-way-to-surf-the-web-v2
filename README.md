# Nightly Panda
A new, modern, accessible, futuristic, funny theme, inspired in Firefox Nightly's design.


If you have any suggestions, tell me in the comments! All the feedback is welcomed! 🦊🔥

![nightly-way-to-surf-the-web-v2](nightlywaytheme08-11-2021.png)
